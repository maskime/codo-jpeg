# CODO-jpeg

Ce projet est une implémentation simplifiée de l'algorithme JPEG en Python.
Le dépôt est décomposé en deux parties : un notebook Jupyter représentatif
de notre travail ainsi que un dossier `src/` contenant les sources du projet.

## Installation

Pour installer les dépendances, utilisez la commande suivante :

```shell
pip install -r requirements.txt
```

## Utilisation

### Programme

Pour lancer notre outil et le tester, utilisez la commande suivante :

```shell
python main.py FILENAME [QUANT]
```

`FILENAME` étant le path vers une image à compresser / décompresser.
L'output du programme présente uniquement l'image après la décompression.

`QUANT` est l'indice de qualité compris entre 0 et 100, si non indiqué, il prend
la valeur 50 par défaut.

Pour plus de détails, vous pouvez consulter le notebook directement.

### Jupyter


Pour faire fonctionner le notebook correctement, placez vous à la racine du
projet et entrez la commande suivante :

```shell
jupyter notebook
```

Enfin, ouvrez le dossier notebook et ouvrez le seul fichier qui s'y trouve.
