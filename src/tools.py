import numpy as np

quantize_arr = np.array([[16, 11, 10, 16, 24, 40, 51, 61], 
[12, 12, 14, 19, 26, 58, 60, 55],
[14, 13, 16, 24, 40, 57, 69, 56],
[14, 17, 22, 29, 51, 87, 80, 62],
[18, 22, 37, 56, 68, 109, 103, 77],
[24, 35, 55, 64, 81, 104, 113, 92],
[49, 64, 78, 87, 103, 121, 120, 101],
[72, 92, 95, 98, 112, 100, 103, 99]])

quantize_arri_uv = np.array([[17, 18, 24, 47, 99, 99, 99, 99],
[18, 21, 26, 66, 99, 99, 99, 99],
[24, 26, 56, 99, 99, 99, 99, 99],
[47, 66, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99]])

def copy_pixels(image1, image2):
    width, height = image1.size
    pixels = image1.load()
    for y in range(height):
        for x in range(width):
            color = pixels[x, y]
            image2.putpixel((x, y), color)
    return image2

def zigzag(arrays):
    h, v, i = 0, 0, 0
    vmin, hmin = 0, 0
    vmax = arrays.shape[0]
    hmax = arrays.shape[1]
    output = np.zeros((vmax * hmax))

    while ((v < vmax) and (h < hmax)):
        if ((h + v) % 2) == 0:
            if (v == vmin):
                output[i] = arrays[v, h]
                if (h == hmax):
                    v += 1
                else:
                    h += 1                     
                i += 1
            elif ((h == hmax - 1) and (v < vmax)):
                output[i] = arrays[v, h]
                v, i = v + 1, i + 1
            elif ((v > vmin) and (h < hmax - 1)):
                output[i] = arrays[v, h]
                v, h, i = v - 1, h + 1, i + 1
        else:
            if ((v == vmax - 1) and (h <= hmax - 1)):
                output[i] = arrays[v, h]
                h, i = h + 1, i + 1
            elif (h == hmin):
                output[i] = arrays[v, h]
                if (v == vmax - 1):
                    h += 1
                else:
                    v += 1
                i += 1
            elif ((v < vmax - 1) and (h > hmin)):
                output[i] = arrays[v, h]
                v, h, i = v + 1, h - 1, i + 1
        if ((v == vmax - 1) and (h == hmax - 1)):
            output[i] = arrays[v, h]
            break

    output = output.astype(np.int16)
    return output

def revert_zigzag(input, vmax, hmax):
    h = 0
    v = 0
    vmin = 0
    hmin = 0
    output = np.zeros((vmax, hmax))
    i = 0

    while ((v < vmax) and (h < hmax)): 
        if ((h + v) % 2) == 0:
            if (v == vmin):
                output[v, h] = input[i]
                if (h == hmax):
                    v = v + 1
                else:
                    h = h + 1                        
                i = i + 1
            elif ((h == hmax -1 ) and (v < vmax)):
                output[v, h] = input[i] 
                v = v + 1
                i = i + 1
            elif ((v > vmin) and (h < hmax -1 )):
                #print(3)
                output[v, h] = input[i] 
                v = v - 1
                h = h + 1
                i = i + 1
        else:
            if ((v == vmax -1) and (h <= hmax -1)):
                output[v, h] = input[i] 
                h = h + 1
                i = i + 1
            elif (h == hmin):
                output[v, h] = input[i] 
                if (v == vmax -1):
                    h = h + 1
                else:
                    v = v + 1
                i = i + 1
            elif((v < vmax -1) and (h > hmin)):
                output[v, h] = input[i] 
                v = v + 1
                h = h - 1
                i = i + 1
        if ((v == vmax-1) and (h == hmax-1)):
            output[v, h] = input[i] 
    return output

def rle_encode(sequence):
    count = 1
    result = []
    for x,item in enumerate(sequence): 
        if x == 0:
            continue
        elif item == sequence[x - 1]:
            count += 1
        else:        
            result.append((sequence[x - 1], count))
            count = 1            
    
    result.append((sequence[len(sequence) - 1], count))
    return result

def rle_decode(sequence):
    result = []
    for item in sequence:
        result.append(item[0] * item[1])

    return "".join(result)
