from src.huffman import *
from src.tools import *

from scipy.fftpack import idct

def decode_image(binary):
    res = []
    for arr in binary:
        tmp = decode_huffman(arr, AC, LUMINANCE)
        tmp = revert_zigzag(tmp, 8, 8)
        res.append(tmp)
    return res

def revert_quantization(arrays, q = 50):
    if q != 50:
        a = 5000 / q if q < 50 else 200 - 2 * q
        for i in range(len(arrays)):
            arrays[i] = np.multiply(arrays[i], (quantize_arr * a + 50) / 100)
    else:
        for i in range(len(arrays)):
            arrays[i] = np.multiply(arrays[i], quantize_arr)

def copy_block(mat, block, x, y):
    for row in range(8):
        for col in range(8):
            mat[y + row, x + col] = block[row, col]
    return mat

def revert_dct(arrays, size):
    res = []
    for arr in arrays:
        tmp = idct(arr, axis=0, norm='ortho')
        tmp += 128
        tmp = tmp.astype(np.int16)
        res.append(tmp)

    mat = np.zeros(dtype=np.uint8, shape=(size[1], size[0]))
    x, y = 0, 0
    for i in range(len(res)):
        mat = copy_block(mat, res[i], x, y)
        if x + 8 >= size[0]:
            x, y = 0, y + 8
        else:
            x += 8

    return mat


def add_color(image, color):
    width, height = image.size
    image = image.convert('RGBA')
    pixels = image.load()

    for y in range(height):
        for x in range(width):
            if color == 'R':
                image.putpixel((x, y), (pixels[x, y][0], 0, 0, 255))
            if color == 'G':
                image.putpixel((x, y), (0, pixels[x, y][1], 0, 255))
            if color == 'B':
                image.putpixel((x, y), (0, 0, pixels[x, y][2], 255))
    return image

def build_image(red, green, blue):
    width, height = red.size
    pixels_r = red.load()
    pixels_g = green.load()
    pixels_b = blue.load()

    for y in range(height):
        for x in range(width):
            r, g, b = pixels_r[x, y][0], pixels_g[x, y][1], pixels_b[x, y][2]
            red.putpixel((x, y), (r, g, b, 255))
    return red
