from PIL import Image, ImageOps
import numpy as np
from scipy.fftpack import dct
from src.huffman import *

from src.tools import *

def rgb_separator(image, primary_color):
    """Create an image with only shades of the specified primary color
    """
    width, height = image.size
    pixels = image.load()
    res = Image.new('RGBA', image.size)

    for y in range(height):
        for x in range(width):
            r, g, b, a = pixels[x, y]
            if primary_color == 'R':
                res.putpixel((x, y), (r, 0, 0, a))
            elif primary_color == 'G':
                res.putpixel((x, y), (0, g, 0, a))
            elif primary_color == 'B':
                res.putpixel((x, y), (0, 0, b, a))
    return res

def padding(image):
    width, height = image.size
    padding_h = 8 - height % 8
    padding_w = 8 - width % 8

    res = Image.new('RGBA', (width + padding_w, height + padding_h))
    res = copy_pixels(image, res)

    pixels = res.load()
    for pad in range(padding_w):
        x = width + pad
        for y in range(height + padding_h):
            px = pixels[width - 1 - pad, y]
            res.putpixel((x, y), px)

    pixels = res.load()
    for pad in range(padding_h):
        y = height + pad
        for x in range(width + padding_w):
            px = pixels[x, height - 1 - pad]
            res.putpixel((x, y), px)

    return res

def dct_all(image):
    arr = np.asarray(image)
    rows, cols = arr.shape
    res = []

    for i in range(0, rows, 8):
        for j in range(0, cols, 8):
            tmp = arr[i:i+8, j:j+8]
            tmp = tmp.astype(np.int16)
            tmp -= 128
            tmp = dct(tmp, axis=0, norm='ortho')
            res.append(tmp)
    return res

def quantization(arrays, q = 50):
    if q != 50:
        a = 5000 / q if q < 50 else 200 - 2 * q
        for i in range(len(arrays)):
            arrays[i] = np.round(np.divide(arrays[i], (quantize_arr * a + 50) / 100))
            arrays[i] = arrays[i].astype(np.int16)
    else:
        for i in range(len(arrays)):
            arrays[i] = np.round(np.divide(arrays[i], quantize_arr))
            arrays[i] = arrays[i].astype(np.int16)

def encode_image(arrays):
    res = []
    for arr in arrays:
        tmp = zigzag(arr)
        tmp = encode_huffman(tmp, LUMINANCE)
        res.append(tmp)
    return res
