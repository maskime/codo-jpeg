from PIL import Image
import numpy as np

from src.compression import *
from src.decompression import *

class JPEG:
    def __init__(self, filename, quant):
        self.filename = filename
        self.quant = quant
        self.binary_r = None
        self.binary_g = None
        self.binary_b = None
        self.size = (-1, -1)

    def compression(self):
        image = Image.open(self.filename)
        self.size = image.size

        # STEP 1
        image = padding(image)
        self.size = image.size

        red = rgb_separator(image, 'R').convert('L')
        green = rgb_separator(image, 'G').convert('L')
        blue = rgb_separator(image, 'B').convert('L')

        # STEP 2
        blocks_r = dct_all(red)
        blocks_g = dct_all(green)
        blocks_b = dct_all(blue)

        # STEP 3
        quantization(blocks_r, self.quant)
        quantization(blocks_g, self.quant)
        quantization(blocks_b, self.quant)

        # STEP 4
        self.binary_r = encode_image(blocks_r)
        self.binary_g = encode_image(blocks_g)
        self.binary_b = encode_image(blocks_b)

        image.close()

    def decompression(self):
        # STEP 1
        blocks_r = decode_image(self.binary_r)
        blocks_g = decode_image(self.binary_g)
        blocks_b = decode_image(self.binary_b)

        # STEP 2
        revert_quantization(blocks_r, self.quant)
        revert_quantization(blocks_g, self.quant)
        revert_quantization(blocks_b, self.quant)

        # STEP 3
        mat_red = revert_dct(blocks_r, self.size)
        red = Image.fromarray(mat_red) 

        mat_green = revert_dct(blocks_g, self.size)
        green = Image.fromarray(mat_green) 

        mat_blue = revert_dct(blocks_b, self.size)
        blue = Image.fromarray(mat_blue) 

        # STEP 4
        red = add_color(red, 'R')
        green = add_color(green, 'G')
        blue = add_color(blue, 'B')

        image = build_image(red, green, blue)
        image.show()
