import sys, os

from src.jpeg import JPEG

def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        raise Exception("Usage: python main.py [FILENAME]")

    filename = sys.argv[1]
    quant = int(sys.argv[2]) if len(sys.argv) == 3 else 50
    jpeg = JPEG(filename, quant)
    jpeg.compression()
    jpeg.decompression()

if __name__ == "__main__":
    main()
